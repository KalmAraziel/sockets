import { Injectable } from '@angular/core';
import { WebsocketService } from './websocket.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(
    public wsService: WebsocketService,
    public http: HttpClient
  ) { }

    sendMessage( mensaje: string ) {
      const payload = {
        de: this.wsService.usuario.nombre,
        cuerpo: mensaje
      };
      this.wsService.emit('mensaje', payload );
    }

    getMessages() {
      return this.wsService.listen('mensaje-nuevo');
    }

    getMessagesPrivate() {
      return this.wsService.listen('mensaje-privado');
    }

    /*
    sushi() {
      this.http.get('http://20.10.10.63:8080/sushi/pedidos/obtener/pedidoslocal/-1/-1/-1/-1/1').subscribe( (data) => {
        console.log(data);
      });
    }
    */

}
