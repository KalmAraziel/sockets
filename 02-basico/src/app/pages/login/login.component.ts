import { Component, OnInit } from '@angular/core';
import { WebsocketService } from 'src/app/services/websocket.service';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  nombre = '';
  constructor(public ws: WebsocketService, private router: Router) { }

  ngOnInit() {
  }

  ingresar() {
    // console.log(this.chat.sushi());
    if (this.nombre.length === 0) {
      return;
    }

    this.ws.loginWS(this.nombre).then( () => {
      this.router.navigateByUrl('/mensajes');
    });

  }
}
